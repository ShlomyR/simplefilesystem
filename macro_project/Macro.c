#include <stdio.h>

#define area_of_a_cube(a) (6*(a*a))

#define inverse_square_root(a) (1/(square root(a)))

#define Area_of_a_cylinder(r,h) ((2/3*(pi))*((r*r)*h))

int main()
{
    printf("SUM using macro: %d\n", area_of_a_cube(5));
    printf("SUM using macro: %d\n", inverse_square_root(5));
    printf("SUM using macro: %d\n", Area_of_a_cylinder(5,7);
    
    return 0;
}


